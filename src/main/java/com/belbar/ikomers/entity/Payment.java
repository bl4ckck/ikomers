package com.belbar.ikomers.entity;

import com.belbar.ikomers.util.DateUtil;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "payments")
public class Payment {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id = UUID.randomUUID();
    private UUID orderId = UUID.randomUUID();

    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    private String paymentType;
    private String status= "pending";
    private Date expiredTime = DateUtil.addHoursToJavaUtilDate(new Date(), 2);
    private Double total;
}
