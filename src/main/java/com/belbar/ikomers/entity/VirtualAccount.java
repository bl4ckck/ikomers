package com.belbar.ikomers.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "virtual_accounts")
@NoArgsConstructor
public class VirtualAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id; // same as customer ID

    @Column(name = "target_fund")
    private Double targetFund = 0D;

    private Double fund = 0D;

    @Column(name = "is_active")
    private Boolean isActive = false;
}