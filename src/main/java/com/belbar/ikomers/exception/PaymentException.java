package com.belbar.ikomers.exception;

public class PaymentException extends Exception {
    public PaymentException(String message) {
        super(message);
    }
}
