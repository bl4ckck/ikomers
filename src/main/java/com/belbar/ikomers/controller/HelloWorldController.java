package com.belbar.ikomers.controller;

import com.belbar.ikomers.dto.HelloWorldDto;
import com.belbar.ikomers.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hello")
public class HelloWorldController {
    @Autowired
    private KafkaTemplate<String, Customer> template;

    @GetMapping("/world")
    public ResponseEntity<HelloWorldDto> getHelloWorld() {
        HelloWorldDto helloWorldDto = new HelloWorldDto("hello");
        return new ResponseEntity<>(helloWorldDto, HttpStatus.OK);
    }

    @PostMapping(path = "/send/foo/{what}")
    public void sendFoo(@PathVariable String what) {
        this.template.send("topic1", "awe", new Customer(100L, what, 22));
    }
}
