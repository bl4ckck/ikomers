package com.belbar.ikomers.controller;

import com.belbar.ikomers.dto.CheckoutSaveDto;
import com.belbar.ikomers.dto.PaymentSaveDto;
import com.belbar.ikomers.entity.Payment;
import com.belbar.ikomers.service.PaymentGatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    private PaymentGatewayService paymentGatewayService;

    @PostMapping("/checkout")
    public ResponseEntity<Payment> checkout(@RequestBody CheckoutSaveDto payload) {
        try {
            return new ResponseEntity<>(this.paymentGatewayService.checkout(payload), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/payment")
    public ResponseEntity<Payment> payment(@RequestBody PaymentSaveDto payload) {
        try {
            return new ResponseEntity<>(this.paymentGatewayService.payment(payload), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
