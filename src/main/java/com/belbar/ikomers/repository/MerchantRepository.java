package com.belbar.ikomers.repository;

import com.belbar.ikomers.entity.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface MerchantRepository extends JpaRepository<Merchant, Long> {
}