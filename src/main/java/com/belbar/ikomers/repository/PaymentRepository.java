package com.belbar.ikomers.repository;

import com.belbar.ikomers.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@RepositoryRestResource(exported = false)
public interface PaymentRepository extends JpaRepository<Payment, UUID> {
    @Transactional
    @Modifying
    @Query("update Payment p set p.status = ?1 where p.expiredTime < ?2 and p.status = ?3")
    void updateStatusByExpiredTimeLessThanAndStatus(String status, Date expiredTime, String status1);
}