package com.belbar.ikomers.dto;

public record CheckoutSaveDto(Long customerId, Long merchantId, String paymentType,
                              Double total) {
}