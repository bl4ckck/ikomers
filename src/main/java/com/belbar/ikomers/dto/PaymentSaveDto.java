package com.belbar.ikomers.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PaymentSaveDto {
    private Long customerId;
    private UUID paymentId;
    private Double fund;
}
