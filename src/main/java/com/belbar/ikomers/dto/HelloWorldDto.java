package com.belbar.ikomers.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HelloWorldDto {
    private String hello;
}
