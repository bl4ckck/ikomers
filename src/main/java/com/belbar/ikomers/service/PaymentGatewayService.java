package com.belbar.ikomers.service;

import com.belbar.ikomers.dto.CheckoutSaveDto;
import com.belbar.ikomers.dto.PaymentSaveDto;
import com.belbar.ikomers.entity.Customer;
import com.belbar.ikomers.entity.Merchant;
import com.belbar.ikomers.entity.Payment;
import com.belbar.ikomers.exception.PaymentException;
import com.belbar.ikomers.repository.PaymentRepository;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static com.belbar.ikomers.service.handler.kafka.KafkaConstant.*;

@Slf4j
@Service
public class PaymentGatewayService {
    @Getter
    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private KafkaTemplate<String, Payment> template;

    // step 1: checkout => payment created and status is pending/deny
    public Payment checkout(CheckoutSaveDto payload) {
        Payment payment = new Payment();
        Customer customer = this.customerService.findByIdCustomer(payload.customerId());
        Merchant merchant = this.merchantService.findByIdMerchant(payload.merchantId());
        payment.setCustomer(customer);
        payment.setMerchant(merchant);
        this.mapper(payment, payload);

        log.info("ini dari service: {}", payment);
        this.template.send(TOPIC_PAYMENT, KEY_CHECKOUT, payment);
        return payment;
    }

    // step 2: mock payment process (e.g tf bank, e-wallet, etc)
    // => updated status to paid/deny
    public Payment payment(PaymentSaveDto payload) throws PaymentException {
        Payment paymentDetail = this.findByIdPayment(payload.getPaymentId());
        this.paymentValidation(payload, paymentDetail);
        // send msg to bankListener VA payment
        this.template.send(TOPIC_BANK, KEY_PAYMENT, paymentDetail);
        return paymentDetail;
    }

    private void paymentValidation(PaymentSaveDto payload, Payment paymentDetail) throws PaymentException {
        // validation
        this.paymentGeneralValidation(paymentDetail, true);
        this.paymentFraudValidation(paymentDetail, payload, true);
        if (!payload.getFund().equals(paymentDetail.getTotal())) {
            throw new PaymentException("Not balanced");
        }
    }

    public Payment findByIdPayment(UUID id) throws PaymentException {
        Optional<Payment> payment = this.paymentRepository.findById(id);
        if (payment.isPresent()) {
            return payment.get();
        }
        throw new PaymentException("Payment ID not found");
    }

    // TODO: update payload kafka, add isError, error msg
    public void paymentFraudValidation(Payment payment, PaymentSaveDto paymentSaveDto,
                                       boolean showException) throws PaymentException {
        if (!payment.getCustomer().getId().equals(paymentSaveDto.getCustomerId())) {
            payment.setStatus("deny");
            if (showException) throw new PaymentException("Customer not same :v");
        }
    }

    public void paymentGeneralValidation(Payment payment, boolean showException) throws PaymentException {
        Date date = new Date();

        if (payment.getTotal() < 0) {
            throw new PaymentException("Total can't less than 0");
        }
        if (payment.getExpiredTime().before(date)) {
            payment.setStatus("expired");
            if (showException) throw new PaymentException("Payment was expired :(");
        }
    }

    private void mapper(Payment target, CheckoutSaveDto source) {
        target.setPaymentType(source.paymentType());
        target.setTotal(source.total());
    }
}
