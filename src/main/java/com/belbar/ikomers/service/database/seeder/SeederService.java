package com.belbar.ikomers.service.database.seeder;

import com.belbar.ikomers.service.handler.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("seederService")
public class SeederService implements Runner<String> {
    @Autowired
    private CustomerSeeder customerSeeder;

    @Autowired
    private MerchantSeeder merchantSeeder;

    @Autowired
    private VirtualAccountSeeder virtualAccountSeeder;

    @Override
    public boolean execute(String ddlType) {
        if (ddlType.equals("create")) {
            this.customerSeeder.seed();
            this.merchantSeeder.seed();
            this.virtualAccountSeeder.seed();
            return true;
        }
        return false;
    }
}
