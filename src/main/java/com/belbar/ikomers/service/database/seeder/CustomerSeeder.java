package com.belbar.ikomers.service.database.seeder;

import com.belbar.ikomers.entity.Customer;
import com.belbar.ikomers.repository.CustomerRepository;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@EqualsAndHashCode(callSuper = true)
@Service
public class CustomerSeeder extends SeederBase<Customer, Long, CustomerRepository> {
    @Transactional
    @Override
    public void seed() {
        for (int i = 0; i < this.totalRow; i++) {
            Customer customer = new Customer();
            customer.setName(this.faker.name().fullName());
            customer.setAge(this.faker.number().numberBetween(12, 50));
            this.dataList.add(customer);
        }
        this.repository.saveAll(this.dataList);
    }
}
