package com.belbar.ikomers.service.database.seeder;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

public abstract class SeederBase<E, I, T extends JpaRepository<E, I>> {
    protected final int totalRow = 10;

    @Autowired
    protected T repository;

    protected final List<E> dataList = new ArrayList<>();

    @Autowired
    protected Faker faker;

    public abstract void seed();
}
