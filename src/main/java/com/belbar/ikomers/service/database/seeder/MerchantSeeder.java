package com.belbar.ikomers.service.database.seeder;

import com.belbar.ikomers.entity.Merchant;
import com.belbar.ikomers.repository.MerchantRepository;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@EqualsAndHashCode(callSuper = true)
@Service
public class MerchantSeeder extends SeederBase<Merchant, Long, MerchantRepository> {
    @Transactional
    @Override
    public void seed() {
        for (int i = 0; i < this.totalRow; i++) {
            Merchant merchant = new Merchant();
            merchant.setName(this.faker.commerce().productName());
            merchant.setBankName(this.faker.company().name());
            merchant.setBankAccount(""+this.faker.number().numberBetween(12345678, 99999999));
            this.dataList.add(merchant);
        }
        this.repository.saveAll(this.dataList);
    }
}
