package com.belbar.ikomers.service.database.seeder;

import com.belbar.ikomers.entity.VirtualAccount;
import com.belbar.ikomers.repository.VirtualAccountRepository;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@EqualsAndHashCode(callSuper = true)
@Service
public class VirtualAccountSeeder extends SeederBase<VirtualAccount, Long, VirtualAccountRepository> {
    @Transactional
    @Override
    public void seed() {
        for (int i = 0; i < this.totalRow; i++) {
            VirtualAccount virtualAccount = new VirtualAccount();
            this.dataList.add(virtualAccount);
        }
        this.repository.saveAll(this.dataList);
    }
}
