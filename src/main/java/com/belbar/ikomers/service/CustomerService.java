package com.belbar.ikomers.service;

import com.belbar.ikomers.entity.Customer;
import com.belbar.ikomers.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public Customer findByIdCustomer(Long id) {
        Optional<Customer> customer = this.customerRepository.findById(id);
        if (customer.isPresent()) {
            return customer.get();
        }
        throw new RuntimeException("Customer not found");
    }
}
