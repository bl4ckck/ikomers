package com.belbar.ikomers.service;

import com.belbar.ikomers.entity.Payment;
import com.belbar.ikomers.entity.VirtualAccount;
import com.belbar.ikomers.repository.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static com.belbar.ikomers.service.handler.kafka.KafkaConstant.KEY_SETTLEMENT;
import static com.belbar.ikomers.service.handler.kafka.KafkaConstant.TOPIC_BANK;

@Slf4j
@Service
public class BankService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private VirtualAccountService virtualAccountService;

    @Autowired
    private PaymentGatewayService paymentGatewayService;

    @Autowired
    private KafkaTemplate<String, Payment> template;

    public void paidProcess(Long vaId, Payment payment) {
        try {
            VirtualAccount virtualAccount = this.virtualAccountService.findByIdVirtualAccount(vaId);
            this.paymentGatewayService.paymentGeneralValidation(payment, true);
            this.virtualAccountService.checkVA(virtualAccount);
            virtualAccount.setFund(payment.getTotal());
            payment.setStatus("paid");
        } catch (Exception e) {
            log.info("failed paid process");
            payment.setStatus("deny");
        } finally {
            this.paymentRepository.save(payment);
            this.template.send(TOPIC_BANK, KEY_SETTLEMENT, payment);
        }
    }
}
