package com.belbar.ikomers.service;

import com.belbar.ikomers.entity.VirtualAccount;
import com.belbar.ikomers.exception.PaymentException;
import com.belbar.ikomers.repository.VirtualAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VirtualAccountService {
    @Autowired
    private VirtualAccountRepository virtualAccountRepository;

    public void generateVA(VirtualAccount virtualAccount) {
        virtualAccount.setIsActive(true);
        this.virtualAccountRepository.save(virtualAccount);
    }

    public void resetVA(Long vaId) {
        VirtualAccount virtualAccount = new VirtualAccount(vaId, 0D, 0D, false);
        this.virtualAccountRepository.save(virtualAccount);
    }

    public void checkVA(VirtualAccount virtualAccount) throws PaymentException {
        if (Boolean.FALSE.equals(virtualAccount.getIsActive())) {
            throw new PaymentException("Virtual account is not active");
        }
    }

    public VirtualAccount findByIdVirtualAccount(Long id) throws PaymentException {
        Optional<VirtualAccount> virtualAccount = this.virtualAccountRepository.findById(id);
        if (virtualAccount.isPresent()) {
            return virtualAccount.get();
        }
        throw new PaymentException("VirtualAccount not found");
    }
}
