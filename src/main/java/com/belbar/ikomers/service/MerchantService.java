package com.belbar.ikomers.service;

import com.belbar.ikomers.entity.Merchant;
import com.belbar.ikomers.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MerchantService {
    @Autowired
    private MerchantRepository merchantRepository;

    public Merchant findByIdMerchant(Long id) {
        Optional<Merchant> merchant = this.merchantRepository.findById(id);
        if (merchant.isPresent()) {
            return merchant.get();
        }
        throw new RuntimeException("Merchant not found");
    }
}
