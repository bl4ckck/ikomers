package com.belbar.ikomers.service.handler;

public interface Runner<T> {
    boolean execute(T param);
}
