package com.belbar.ikomers.service.handler.kafka;

import com.belbar.ikomers.entity.Payment;
import com.belbar.ikomers.service.BankService;
import com.belbar.ikomers.service.PaymentGatewayService;
import com.belbar.ikomers.service.VirtualAccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import static com.belbar.ikomers.service.handler.kafka.KafkaConstant.*;

@Slf4j
@Configuration
public class BankListenerHandler implements KafkaListenerHandler<Payment> {
    @Autowired
    private PaymentGatewayService paymentGatewayService;

    @Autowired
    private VirtualAccountService virtualAccountService;

    @Autowired
    private BankService bankService;

    @KafkaListener(id = "blbrBankSettlementGroup", topics = TOPIC_BANK)
    @Override
    public void listen(@Payload Payment payment, @Header(KafkaHeaders.RECEIVED_KEY) String key) {
        if (key.equals(KEY_SETTLEMENT)) {
            this.settlementProcess(payment);
            this.reimbursementMerchant();
        }
    }

    @KafkaListener(id = "blbrBankDltGroup", topics = TOPIC_BANK)
    @Override
    public void dltListen(@Payload Payment payment, @Header(KafkaHeaders.RECEIVED_KEY) String key) {
        log.info("Received from DLT Bank: {}", payment.getId());
    }

    @KafkaListener(id = "blbrBankGroup", topics = TOPIC_BANK)
    public void listenVAPayment(@Payload Payment payment, @Header(KafkaHeaders.RECEIVED_KEY) String key) {
        if (key.equals(KEY_PAYMENT)) {
            log.info("Received bank va payment ID: {}", payment.getId());
            this.bankService.paidProcess(payment.getCustomer().getId(), payment);
        }
    }

    private void settlementProcess(Payment payment) {
        log.info("Received bank settlement ID: {}", payment.getId());

        if (!payment.getStatus().equals("deny")) {
            try {
                this.paymentGatewayService.paymentGeneralValidation(payment, true);
                /**
                 * TODO:
                 * - reimburse to merchant
                 */
                payment.setStatus("settlement");
            } catch (Exception e) {
                log.info("failed settlement process");
                payment.setStatus("refund");
            } finally {
                this.paymentGatewayService.getPaymentRepository().save(payment);
                this.virtualAccountService.resetVA(payment.getCustomer().getId());
            }
        }
    }

    private void reimbursementMerchant() {
        log.info("Reimbursement completed");
    }
}
