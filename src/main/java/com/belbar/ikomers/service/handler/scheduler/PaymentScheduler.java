package com.belbar.ikomers.service.handler.scheduler;

import com.belbar.ikomers.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

// TODO: migrate to kafka processor context
@Component
public class PaymentScheduler {
    @Autowired
    private PaymentRepository paymentRepository;

    @Scheduled(fixedRate = 10000) // 10s; real case should be 24h
    public void checkExpiredTimePayment() {
        Date date = new Date();
        this.paymentRepository.updateStatusByExpiredTimeLessThanAndStatus("expired", date, "pending");
    }
}
