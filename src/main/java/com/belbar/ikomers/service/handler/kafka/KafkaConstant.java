package com.belbar.ikomers.service.handler.kafka;

public class KafkaConstant {
    public static final String TOPIC_PAYMENT = "payment";
    public static final String TOPIC_BANK = "bank";
    public static final String KEY_CHECKOUT = "blblr-checkout";
    public static final String KEY_PAYMENT = "blblr-payment";
    public static final String KEY_SETTLEMENT = "blbr-settlement";
}
