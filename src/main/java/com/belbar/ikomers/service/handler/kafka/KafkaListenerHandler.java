package com.belbar.ikomers.service.handler.kafka;

import com.belbar.ikomers.exception.PaymentException;

public interface KafkaListenerHandler<T> {
    void listen(T msg, String key) throws PaymentException;
    void dltListen(T err, String key);
}
