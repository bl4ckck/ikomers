package com.belbar.ikomers.service.handler.kafka;

import com.belbar.ikomers.entity.Payment;
import com.belbar.ikomers.entity.VirtualAccount;
import com.belbar.ikomers.service.PaymentGatewayService;
import com.belbar.ikomers.service.VirtualAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import static com.belbar.ikomers.service.handler.kafka.KafkaConstant.KEY_CHECKOUT;

@Slf4j
@Configuration
public class PaymentGatewayListenerHandler implements KafkaListenerHandler<Payment> {
    @Autowired
    private PaymentGatewayService paymentGatewayService;

    @Autowired
    private VirtualAccountService virtualAccountService;

    @KafkaListener(id = "blbrPaymentGroup", topics = "payment")
    @Override
    public void listen(@Payload Payment payment, @Header(KafkaHeaders.RECEIVED_KEY) String key) {
        this.checkoutProcess(key, payment);
    }

    @KafkaListener(id = "blbrPaymentDltGroup", topics = "payment.DLT")
    @Override
    public void dltListen(@Payload Payment payment, @Header(KafkaHeaders.RECEIVED_KEY) String key) {
        log.info("Received from DLT Payment: {}", payment.getId());
    }

    private void checkoutProcess(String key, Payment payment) {
        if (key.equals(KEY_CHECKOUT)) {
            log.info("Received payment ID: {}", payment.getId());

            try {
                VirtualAccount va = new VirtualAccount();
                va.setId(payment.getCustomer().getId());
                va.setTargetFund(payment.getTotal());

                this.virtualAccountService.generateVA(va);
                this.paymentGatewayService.paymentGeneralValidation(payment, true);
            } catch (Exception e) {
                log.info("failed checkout");
                payment.setStatus("deny");
            } finally {
                this.paymentGatewayService.getPaymentRepository().save(payment);
            }
        }
    }
}
