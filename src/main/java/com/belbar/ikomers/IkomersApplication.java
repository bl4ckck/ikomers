package com.belbar.ikomers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class IkomersApplication {

	public static void main(String[] args) {
		SpringApplication.run(IkomersApplication.class, args);
	}

}
