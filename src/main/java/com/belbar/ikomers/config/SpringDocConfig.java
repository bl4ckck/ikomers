package com.belbar.ikomers.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringDocConfig {
    @Bean
    public OpenAPI swaggerApi() {
        return new OpenAPI()
                .components(new Components())
                .info(this.getInfo());
    }

    private Info getInfo() {
        return new Info().title("Belbar API")
                .version("v1.0.0")
                .license(this.getLicense());
    }

    private License getLicense() {
        return new License().name("Apache 2.0")
                .url("http://springdoc.org");
    }
}
