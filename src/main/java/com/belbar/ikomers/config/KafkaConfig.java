package com.belbar.ikomers.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.listener.CommonErrorHandler;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.converter.JsonMessageConverter;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.util.backoff.FixedBackOff;

import static com.belbar.ikomers.service.handler.kafka.KafkaConstant.TOPIC_BANK;
import static com.belbar.ikomers.service.handler.kafka.KafkaConstant.TOPIC_PAYMENT;

@Slf4j
@Configuration
public class KafkaConfig {
    /*
     * Boot will autowire this into the container factory.
     */
    @Bean
    public CommonErrorHandler errorHandler(KafkaOperations<Object, Object> template) {
        return new DefaultErrorHandler(
                new DeadLetterPublishingRecoverer(template), new FixedBackOff(1000L, 3));
    }

    @Bean
    public RecordMessageConverter converter() {
        return new JsonMessageConverter();
    }

    @Bean
    public NewTopic topic() {
        return new NewTopic(TOPIC_PAYMENT, 5, (short) 1);
    }

    @Bean
    public NewTopic topicBank() {
        return new NewTopic(TOPIC_BANK, 5, (short) 1);
    }
}
