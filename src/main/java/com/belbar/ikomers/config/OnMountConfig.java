package com.belbar.ikomers.config;

import com.belbar.ikomers.service.handler.Runner;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Configuration
public class OnMountConfig {
    @Autowired
    @Qualifier("seederService")
    private Runner<String> seederService;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String ddlType;

    @Bean
    public ApplicationRunner applicationRunner() {
        return args -> this.seed();
    }

    /**
     * Running seeder when ddl-auto = create
     */
    private void seed() {
        boolean status = this.seederService.execute(this.ddlType);
        if (status) {
            log.info("Seeders completed");
        } else {
            log.info("No running seeders");
        }
    }
}
